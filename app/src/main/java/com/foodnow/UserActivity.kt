package com.foodnow

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.navigation.fragment.NavHostFragment
import kotlinx.android.synthetic.main.activity_user_main.*
import kotlinx.android.synthetic.main.topbarlayout.*
import kotlinx.android.synthetic.main.topbarlayout.view.*


class UserActivity : AppCompatActivity() {


    var navHostFragment: NavHostFragment? = null
    var toolbar: Toolbar? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (InitApplication.getInstance().isNightModeEnabled) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }
        setContentView(R.layout.activity_user_main)
        //binding = DataBindingUtil.setContentView(this,R.layout.activity_user_main)


       // MyApp.setCurrentActivity(this@LoginSignupActivity)
        navHostFragment =
            supportFragmentManager.findFragmentById(R.id.splash_host) as NavHostFragment
        toolbar = toplayout.toolbar
        navHostFragment!!.navController.navigate(R.id.splashFragment)


    }

    fun setDarkMode(isChecked: Boolean){
        if (isChecked) {
            InitApplication.getInstance().setIsNightModeEnabled(true)
            val intent = intent
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            finish()
            startActivity(intent)
        } else {
            InitApplication.getInstance().setIsNightModeEnabled(false)
            val intent = intent
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            finish()
            startActivity(intent)
        }
    }

    fun hideToolBar() {
        toplayout.visibility = View.GONE
    }

    fun setToolbarTitle(title: String) {
       toplayout.visibility = View.VISIBLE
        toolbar_title.text = title
        //binding!!.toplayout.imgDone.visibility = View.GONE
    }

}