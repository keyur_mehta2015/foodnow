package com.foodnow


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

class RestaurantFragment : Fragment() {
    companion object {
        fun newInstance(): RestaurantFragment {
            return RestaurantFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.restaurant_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as BaseActivity).setToolbarTitle("vv")
        /*(activity as BaseActivity).toolbar!!.setNavigationOnClickListener {
            activity!!.onBackPressed()
        }*/
        //binding!!.lifecycleOwner = this

    }

}

