package com.foodnow

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import kotlinx.android.synthetic.main.activity_base.*


class BaseActivity : AppCompatActivity() {
    var navController: NavController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

       // MyApp.setCurrentActivity(this@LoginSignupActivity)
        val navHostFragment: NavHostFragment? = supportFragmentManager.findFragmentById(R.id.main_host) as NavHostFragment
        navHostFragment!!.navController.navigate(R.id.restaurantFragment)
        setupNavigation()
    }

    // Setting Up One Time Navigation
    private fun setupNavigation() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowHomeEnabled(false)   //disable back button
        supportActionBar!!.setHomeButtonEnabled(false)
        //supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        navController = Navigation.findNavController(this, R.id.main_host)
        NavigationUI.setupActionBarWithNavController(this, navController!!, drawerLayout)
        NavigationUI.setupWithNavController(navView, navController!!)
       // navView.setNavigationItemSelectedListener(this)
    }

    fun setToolbarTitle(title: String) {
        toolbar_title.text = title
        //binding!!.toplayout.imgDone.visibility = View.GONE
    }

   /* fun hideToolBar() {
        binding!!.toplayout.visibility = View.GONE
    }

    fun setToolbarTitle(title: String) {
        binding!!.toplayout.visibility = View.VISIBLE
        toolbar_title.text = title
        //binding!!.toplayout.imgDone.visibility = View.GONE
    }*/

}