package com.foodnow


import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.confirm_code_fragment.*


class ConfirmCodeFragment : Fragment() {
    companion object {
        fun newInstance(): ConfirmCodeFragment {
            return ConfirmCodeFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.confirm_code_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as UserActivity).setToolbarTitle(activity!!.resources.getString(R.string.sign_in))
        (activity as UserActivity).toolbar!!.setNavigationOnClickListener {
            activity!!.onBackPressed()
        }

        /* code to show keyboard on startup.this code is not working.*/
        edtCode.requestFocus()
        val imm: InputMethodManager? =
            activity!!.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm!!.showSoftInput(edtCode, InputMethodManager.SHOW_IMPLICIT)

        edtCode.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if(edtCode.length() >= 4 ){
                    edtCode.clearFocus()
                    imm.hideSoftInputFromWindow(edtCode.windowToken, 0)

                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })
    }

    fun hideKeyboard(activity: Activity) {
        val imm =
            activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

}

