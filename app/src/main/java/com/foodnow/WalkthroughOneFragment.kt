package com.foodnow


import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.walkthrough_one_fragment.*


class WalkthroughOneFragment(var activity: Activity, var pos: Int) : Fragment() {
    /* companion object {
         fun newInstance(): WalkthroughOneFragment {
             return WalkthroughOneFragment(activity = )
         }
     }*/

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.walkthrough_one_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as UserActivity).hideToolBar()
        /*imgWalk.setImageResource(R.drawable.walk_one)
        txtWalkTitle.text = activity.resources.getString(R.string.walk_one_title)
        txtWalkTitleDis.text = activity.resources.getString(R.string.walk_one_dis)*/
        when (pos) {
            0 -> {
                imgWalk.setImageResource(R.drawable.walk_one)
                txtWalkTitle.text = activity.resources.getString(R.string.walk_one_title)
                txtWalkTitleDis.text = activity.resources.getString(R.string.walk_one_dis)
            }
            1 -> {
                imgWalk.setImageResource(R.drawable.walk_two)
                txtWalkTitle.text = activity.resources.getString(R.string.walk_two_title)
                txtWalkTitleDis.text = activity.resources.getString(R.string.walk_two_dis)
            }
            else -> {
                imgWalk.setImageResource(R.drawable.walk_three)
                txtWalkTitle.text = activity.resources.getString(R.string.walk_three_title)
                txtWalkTitleDis.text = activity.resources.getString(R.string.walk_three_dis)
            }
        }
    }


}

