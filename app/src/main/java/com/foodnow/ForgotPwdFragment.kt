package com.foodnow


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.forgot_pwd_fragment.*

class ForgotPwdFragment : Fragment() {
    companion object {
        fun newInstance(): ForgotPwdFragment {
            return ForgotPwdFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.forgot_pwd_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as UserActivity).hideToolBar()
        imgClose.setOnClickListener {
            activity!!.onBackPressed()
        }
        btnSendPwd.setOnClickListener {
            (activity as UserActivity).navHostFragment!!.findNavController()
                .navigate(R.id.action_move_to_confirm_code)
        }
    }

}

