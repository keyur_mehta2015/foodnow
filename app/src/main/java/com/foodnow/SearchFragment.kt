package com.foodnow


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

class SearchFragment : Fragment() {
    companion object {
        fun newInstance(): SearchFragment {
            return SearchFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.search_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as UserActivity).setToolbarTitle(activity!!.resources.getString(R.string.register_now))
        (activity as UserActivity).toolbar!!.setNavigationOnClickListener {
            activity!!.onBackPressed()
        }
        //binding!!.lifecycleOwner = this

    }

}

