package com.foodnow


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

class OrderNowFragment : Fragment() {
    companion object {
        fun newInstance(): OrderNowFragment {
            return OrderNowFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.ordernow_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as BaseActivity).setToolbarTitle(activity!!.resources.getString(R.string.register_now))
        //binding!!.lifecycleOwner = this

    }

}

