package com.foodnow


import android.content.Intent
import android.content.Intent.getIntent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.signup_fragment.btnSignUp
import kotlinx.android.synthetic.main.spalsh_fragment.*

class SpalshFragment : Fragment() {
    companion object {
        fun newInstance(): SpalshFragment {
            return SpalshFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.spalsh_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as UserActivity).hideToolBar()
        /*(activity as LoginSignupActivity).setToolbarTitle("")
        (activity as LoginSignupActivity).toolbar!!.setNavigationOnClickListener {
            activity!!.onBackPressed()
        }*/
        //binding!!.lifecycleOwner = this

        btnSignUp.setOnClickListener {
            (activity as UserActivity).navHostFragment!!.findNavController()
                .navigate(R.id.action_move_to_signup)
        }

        btnLogin.setOnClickListener {
            (activity as UserActivity).navHostFragment!!.findNavController()
                .navigate(R.id.action_move_to_signin)
        }

        /*txtLogo.setOnClickListener {
            (activity as UserActivity).setDarkMode(true)
        }
        txtLogo.setOnLongClickListener {
            (activity as UserActivity).setDarkMode(false)
            return@setOnLongClickListener false
        }*/

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) switchTheme.isChecked = true

        switchTheme.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                (activity as UserActivity).setDarkMode(true)
            } else {
                (activity as UserActivity).setDarkMode(false)
            }
        })

    }

}

