package com.foodnow

import android.app.Activity
import android.app.Application

class MyApp : Application() {
   private var mCurrentActivity: Activity? = null
    var currentActivity: Activity?
        get() = mCurrentActivity
        set(mCurrentActivity) {
            this.mCurrentActivity = mCurrentActivity
        }

}