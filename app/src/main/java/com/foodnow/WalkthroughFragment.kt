package com.foodnow


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import kotlinx.android.synthetic.main.walkthrough_fragment.*
import java.util.*


class WalkthroughFragment : Fragment() {
    companion object {
        fun newInstance(): WalkthroughFragment {
            return WalkthroughFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.walkthrough_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as UserActivity).hideToolBar()
        val adapter = ViewPagerAdapter(activity!!.supportFragmentManager)
        adapter.addFrag(WalkthroughOneFragment(activity!!,0), "WalkthroughOneFragment")
        adapter.addFrag(WalkthroughOneFragment(activity!!,1), "WalkthroughTwoFragment")
        adapter.addFrag(WalkthroughOneFragment(activity!!,2), "WalkthroughThreeFragment")

        viewpager.adapter = adapter
        ciIndicator.setViewPager(viewpager)

    }

    @Suppress("DEPRECATION")
    private class ViewPagerAdapter(manager: FragmentManager?) :
        FragmentStatePagerAdapter(manager!!) {
        private val mFragmentList: MutableList<Fragment> =
            ArrayList()
        private val mFragmentTitleList: MutableList<String> =
            ArrayList()

        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFrag(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitleList[position]
        }
    }

}

