package com.foodnow


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.signin_fragment.*

class SignInFragment : Fragment() {
    companion object {
        fun newInstance(): SignInFragment {
            return SignInFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.signin_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as UserActivity).setToolbarTitle(activity!!.resources.getString(R.string.sign_in))
        (activity as UserActivity).toolbar!!.setNavigationOnClickListener {
            activity!!.onBackPressed()
        }
        //binding!!.lifecycleOwner = this

        txtTouchHere.setOnClickListener {
            (activity as UserActivity).navHostFragment!!.findNavController()
                .navigate(R.id.action_move_to_forgotPwd)
        }

        btnSignIn.setOnClickListener {
            (activity as UserActivity).navHostFragment!!.findNavController()
                .navigate(R.id.action_move_to_walkthrough)
        }

    }

}

